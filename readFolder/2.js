// READ / WRITE folder
// `npm i -g nodemon`
// `nodemon --inspect 2.js`
// put test data into folder 'data'
// comment input and put output you want
// outputString is output

const fs = require("fs");
const readline = require("readline");

//#region GLOBALS
let globalOutput = "";
const globalOutputFilePath = "./globaloutput";

const files = [];
let filesToProcess = 0;

let caseIndex = 1;
let outputString = "";
//#endregion

//#region HELP FUNCTIONS
const setFilesVar = (path = "./data") => {
  fs.readdirSync(path).forEach((file) => {
    const subpath = path + "/" + file;
    if (fs.lstatSync(subpath).isDirectory()) {
      setFilesVar(subpath);
    } else {
      files.push(path + "/" + file);
    }
  });
};

const processFile = (file, resolve) => {
  caseIndex = 1;
  outputString = "";

  const rl = readline.createInterface({
    // input: process.stdin,
    input: fs.createReadStream(`./${file}`),
    output: process.stdout,
    // output: fs.createWriteStream(`./${file}.out`),
  });

  rl.on("line", processLine).on("close", () => {
    rl.output.write(outputString);
    filesToProcess--;
    filesToProcess === 0 &&
      fs.writeFile(globalOutputFilePath, globalOutput, () => {});
    resolve("");
  });
};
//#endregion

//#region PROCESS
const processLine = (line) => {
  // ------------------------ EXAMPLE
  // put your code in HERE
  let newLine = [];
  for (let i = 0; i < line.length; i++) {
    newLine[line.length - i - 1] = line[i];
  }
  let newLineString = newLine.join("");
  // ------------------------ EXAMPLE

  outputString += `Case #${caseIndex}: ${newLineString}\n`;
  globalOutput += `Case #${caseIndex++}: ${newLineString}\n`;
};
//#endregion

//#region RUN
setFilesVar();
filesToProcess = files.length;

(async () => {
  for (file of files) {
    // console.log(file);
    await new Promise((resolve) => {
      processFile(file, resolve);
    });
    // processFile(file);
  }
})();
//#endregion

// READ / WRITE file
// `npm i -g nodemon`
// `nodemon --inspect 1.js`
// put test data into file '1.in'
// comment input and put output you want
// outputString is output

const fs = require("fs");
const readline = require("readline");

//#region GLOBALS
const inOutFileName = "1";
let caseIndex = 1;
let TestCases;
let Numbers = -1;
let outputString = "";
//#endregion

//#region PROCESS
const processLine = (line) => {
  if (!TestCases) {
    TestCases = +line; // process first line
    return;
  } else if (Numbers < 0) {
    Numbers = +line; // process second line
    return;
  }
  // ------------------- // process any other line
  // put your code in HERE
  // -------------------

  console.log(`Case #${caseIndex++}: ${line}`);
  outputString += `Case #${caseIndex++}: ${line}\n`;
  // clear
  Numbers = -1;
};
//#endregion

//#region HELP FUNCTIONS
const rl = readline.createInterface({
  // input: process.stdin,
  input: fs.createReadStream(`./${inOutFileName}.in`),
  // output: process.stdout,
  output: fs.createWriteStream(`./${inOutFileName}.out`),
});

rl.on("line", processLine).on("close", () => {
  rl.output.write(outputString);
  process.exit(0);
});
//#endregion

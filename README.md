### **Description**

Template for process console/file/files in NodeJS

- read from file or stdin
- write to file or stdout
- proces file by lines
- process all files in given folder

---

### **Technology**

JavaScript, NodeJS

---

### **Year**

2020

---

### **Structure**

- readFile

  - `1.in` // input file
  - `1.js` // source file where input/stdin is processed
  - `1.out` // output file

- readFolder
  - `data` // folder with files and folders
  - `2.js` // source where files from data folder or stdin are processed
  - `globaloutput` // output file (every input file can have its own output too)

---

### **Run**

I use template.js which I created

- comment input and uncomment process.stdin for production

1. option to run script (with debugger)

- with extension https://marketplace.visualstudio.com/items?itemName=miramac.vscode-exec-node&ssr=false#overview
- run it with F8 (focus in file), stop it with F9
- for debbugger set in VS Code in Settings > `node debug` > Auto Attach: On

2. option with hot deploy:

- `npm i -g nodemon`
- `nodemon --inspect 1.js`

---

### **Screenshots**

![](README/main.png)
